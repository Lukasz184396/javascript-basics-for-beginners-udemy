//checking of solutions
let numbers = [1, 2, 3, 4];

const output = move(numbers, 0, 1);

console.log(output);
//  0, 0   => [1, 2, 3, 4];
//  0, 1   => [2, 1, 3, 4];
//  0, 2   => [2, 3, 1, 4];
//  0, 3   => [2, 3, 4, 1];
//  1, -1   => [2, 1, 3, 4];


//functions
function move(array, index, offset) {
    const output = [];
    for (let element of array) {

        if (validateInputOfMoveFunction(index, array, offset)) {
            output[index + offset] =  array[index];
        }
        else return console.error('Invalid input');

    }
    return output;
}

function validateInputOfMoveFunction(index, array, offset) {
    return (index < 0 ||index + offset < 0 || index + offset >= array.length) ? false : true;
}